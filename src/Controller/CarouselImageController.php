<?php

namespace App\Controller;

use App\Entity\CarouselImage;
use App\Form\CarouselImageType;
use App\Service\EventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/carousel/image")
 * @IsGranted("ROLE_ADMIN")
 */
class CarouselImageController extends AbstractController
{
    /**
     * @Route("/", name="carousel_image_index", methods={"GET"})
     */
    public function index(EventService $eventService): Response
    {
        $carouselImages = $this->getDoctrine()
            ->getRepository(CarouselImage::class)
            ->findAll();
        $eventService->dispatchEvent('checked Caroussel images');

        return $this->render('carousel_image/index.html.twig', [
            'carousel_images' => $carouselImages,
        ]);
    }

    /**
     * @Route("/new", name="carousel_image_new", methods={"GET","POST"})
     */
    public function new(Request $request, EventService $eventService): Response
    {
        $carouselImage = new CarouselImage();
        $form = $this->createForm(CarouselImageType::class, $carouselImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($carouselImage);
            $entityManager->flush();
            $eventService->dispatchEvent('added new Carousel image');

            return $this->redirectToRoute('carousel_image_index');
        }

        return $this->render('carousel_image/new.html.twig', [
            'carousel_image' => $carouselImage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="carousel_image_show", methods={"GET"})
     */
    public function show(CarouselImage $carouselImage, EventService $eventService): Response
    {
        $eventService->dispatchEvent('checked Carousel image details');
        return $this->render('carousel_image/show.html.twig', [
            'carousel_image' => $carouselImage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="carousel_image_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CarouselImage $carouselImage, EventService $eventService): Response
    {
        $form = $this->createForm(CarouselImageType::class, $carouselImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $eventService->dispatchEvent('edited a Carousel image');

            return $this->redirectToRoute('carousel_image_index');
        }

        return $this->render('carousel_image/edit.html.twig', [
            'carousel_image' => $carouselImage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="carousel_image_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CarouselImage $carouselImage, EventService $eventService): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carouselImage->getId(), $request->request->get('_token'))) {
            $eventService->dispatchEvent('deleted a Carousel image');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($carouselImage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('carousel_image_index');
    }
}
