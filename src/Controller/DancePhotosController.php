<?php

namespace App\Controller;

use App\Entity\DancePhotos;
use App\Form\DancePhotosType;
use App\Service\EventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("adminMenu/dance/photos")
 */
class DancePhotosController extends AbstractController
{
    /**
     * @Route("/", name="dance_photos_index", methods={"GET"})
     */
    public function index(EventService $eventService): Response
    {
        $dancePhotos = $this->getDoctrine()
            ->getRepository(DancePhotos::class)
            ->findAll();
        $eventService->dispatchEvent('checked dance photos');

        return $this->render('dance_photos/index.html.twig', [
            'dance_photos' => $dancePhotos,
        ]);
    }

    /**
     * @Route("/new", name="dance_photos_new", methods={"GET","POST"})
     */
    public function new(Request $request, EventService $eventService): Response
    {
        $dancePhoto = new DancePhotos();
        $form = $this->createForm(DancePhotosType::class, $dancePhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dancePhoto);
            $entityManager->flush();
            $eventService->dispatchEvent('uploaded a new photo');

            return $this->redirectToRoute('dance_photos_index');
        }

        return $this->render('dance_photos/new.html.twig', [
            'dance_photo' => $dancePhoto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dance_photos_show", methods={"GET"})
     */
    public function show(DancePhotos $dancePhoto, EventService $eventService): Response
    {
        $eventService->dispatchEvent('checked a '.$dancePhoto->getImageName().' details');
        return $this->render('dance_photos/show.html.twig', [
            'dance_photo' => $dancePhoto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dance_photos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DancePhotos $dancePhoto, EventService $eventService): Response
    {
        $form = $this->createForm(DancePhotosType::class, $dancePhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $eventService->dispatchEvent('edited '.$dancePhoto->getImageName());

            return $this->redirectToRoute('dance_photos_index');
        }

        return $this->render('dance_photos/edit.html.twig', [
            'dance_photo' => $dancePhoto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dance_photos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DancePhotos $dancePhoto, EventService $eventService): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dancePhoto->getId(), $request->request->get('_token'))) {
            $eventService->dispatchEvent('deleted '.$dancePhoto->getImageName());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($dancePhoto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dance_photos_index');
    }
}
