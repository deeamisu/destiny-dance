<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Event\AdminLogEvent;
use App\EventSubscriber\AdminLogSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Service\AdminLogSubscriberService;

class AdminSecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(AdminLogSubscriberService $adminLogSubscriberService)
    {
        //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
        $dispatcher = new EventDispatcher();
        $event = new AdminLogEvent('logged out');
        $subscriber = new AdminLogSubscriber($adminLogSubscriberService);
        $dispatcher->addSubscriber($subscriber);
        $dispatcher->dispatch(AdminLogEvent::NAME,$event);

        return $this->redirectToRoute('default');
    }
}
