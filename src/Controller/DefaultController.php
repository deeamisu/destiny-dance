<?php

namespace App\Controller;

use App\Entity\PhotoCategory;
use App\Repository\PhotoCategoryRepository;
use App\Service\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(Admin $adminMneu)
    {
        $images = iterator_to_array($adminMneu->getAllImageFiles(),false);
        $firstImage = array_shift($images);

        return $this->render('default/index.html.twig', [
            'firstImage' => $firstImage, 'images' => $images
        ]);
    }

    /**
     * @Route("/galery", name="galery")
     */
    public function galery(Admin $adminMenu, EntityManagerInterface $entityManager)
    {
        $categories = $entityManager->getRepository(PhotoCategory::class)->findAll();
        return $this->render('default/galery.html.twig',[
            'galery' => $adminMenu->getGaleryPhotosByCategory(),
            'categories' => $categories
        ]);
    }
}
