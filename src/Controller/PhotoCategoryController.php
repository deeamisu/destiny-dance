<?php

namespace App\Controller;

use App\Entity\PhotoCategory;
use App\Form\PhotoCategoryType;
use App\Repository\PhotoCategoryRepository;
use App\Service\EventService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("adminMenu/photo/category")
 */
class PhotoCategoryController extends AbstractController
{
    /**
     * @Route("/", name="photo_category_index", methods={"GET"})
     */
    public function index(PhotoCategoryRepository $photoCategoryRepository, EventService $eventService): Response
    {
        $eventService->dispatchEvent('checked photo categories');
        return $this->render('photo_category/index.html.twig', [
            'photo_categories' => $photoCategoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="photo_category_new", methods={"GET","POST"})
     */
    public function new(Request $request, EventService $eventService): Response
    {
        $photoCategory = new PhotoCategory();
        $form = $this->createForm(PhotoCategoryType::class, $photoCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($photoCategory);
            $entityManager->flush();
            $eventService->dispatchEvent('added new photo category');

            return $this->redirectToRoute('photo_category_index');
        }

        return $this->render('photo_category/new.html.twig', [
            'photo_category' => $photoCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="photo_category_show", methods={"GET"})
     */
    public function show(PhotoCategory $photoCategory, EventService $eventService): Response
    {
        $eventService->dispatchEvent('checked '.$photoCategory->getName().' details');
        return $this->render('photo_category/show.html.twig', [
            'photo_category' => $photoCategory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="photo_category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PhotoCategory $photoCategory, EventService $eventService): Response
    {
        $form = $this->createForm(PhotoCategoryType::class, $photoCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $eventService->dispatchEvent('edited '.$photoCategory->getName());

            return $this->redirectToRoute('photo_category_index');
        }

        return $this->render('photo_category/edit.html.twig', [
            'photo_category' => $photoCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="photo_category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PhotoCategory $photoCategory, EventService $eventService): Response
    {
        if ($this->isCsrfTokenValid('delete'.$photoCategory->getId(), $request->request->get('_token'))) {
            $eventService->dispatchEvent('deleted '.$photoCategory->getName().' category');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($photoCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('photo_category_index');
    }
}
