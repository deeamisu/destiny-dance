<?php

namespace App\Controller;

use App\Entity\CarouselImage;
use App\Entity\Member;
use App\Form\MembersType;
use App\Service\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/adminMenu")
 */
class AdminMenuController extends AbstractController
{
    /**
     * @Route("/carousel", name="admin_menu_carousel")
     */
    public function index(Admin $adminMenu, EntityManagerInterface $entityManager)
    {
        $carouselImages = $entityManager->getRepository(CarouselImage::class)->findAll();
        return $this->render('admin_menu/index.html.twig', [
            'files' => $carouselImages,
        ]);
    }

    /**
     * @Route("/members", name="admin_menu_members")
     */
    public function showMembers(EntityManagerInterface $entityManager)
    {
        $members = $entityManager->getRepository(Member::class)->findAll();

        return $this->render('admin_menu/members.html.twig', [
           'members' => $members
        ]);
    }

    /**
     * @Route("/PDF", name="admin_menu_registration", methods={"POST"})
     */
    public function registrationPDF(EntityManagerInterface $entityManager)
    {
        $date = date('d.m.Y');
        $activeIds = $_POST['inclass'];
        $activeMembers = [];
        foreach ($activeIds as $id)
        {
            $activeMembers[] = $entityManager->getRepository(Member::class)->find($id);
        }

        /**return $this->render('admin_menu/inclass_members.html.twig',[
            'members' => $activeMembers, 'date' => $date,
        ]);*/

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('admin_menu/inclass_members.html.twig',[
            'members' => $activeMembers, 'date' => $date,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
        $dompdf->stream("prezenta".$date.".pdf", [
            "Attachment" => false
        ]);
    }
}
