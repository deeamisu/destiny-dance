<?php

namespace App\EventSubscriber;

use App\Service\AdminLogSubscriberService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use App\Event\AdminLogEvent;

class AdminLogSubscriber implements EventSubscriberInterface
{
    private $adminLogService;

    /**
     * AdminLogSubscriber constructor.
     * @param $adminLogService AdminLogSubscriberService
     */
    public function __construct(AdminLogSubscriberService $adminLogService)
    {
        $this->adminLogService = $adminLogService;
    }


    public static function getSubscribedEvents()
    {
        return [
            'security.interactive_login' => 'onSecurityAuthenticationSuccess',
            'admin.action' => 'onAdminAction'
        ];
    }

    public function onSecurityAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        $this->adminLogService->setRecordLogin();
    }

    public function onAdminAction(AdminLogEvent $event)
    {
        $this->adminLogService->adminAction($event->getMessage());
    }
}
