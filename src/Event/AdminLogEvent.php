<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/21/2020
 * Time: 2:34 AM
 */

namespace App\Event;

use App\Entity\Admin;
use Symfony\Contracts\EventDispatcher\Event;


class AdminLogEvent extends Event
{
    const NAME = 'admin.action';

    private $message;

    /**
     * AdminLogEvent constructor.
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

}