<?php

namespace App\Command;

use App\Service\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MembersImportCommand extends Command
{
    protected static $defaultName = 'app:members-import';

    private $adminMenu;

    private $entityManager;

    /**
     * MembersImportCommand constructor.
     * @param $adminMenu
     * @param $entityManager
     */
    public function __construct(Admin $adminMenu, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->adminMenu = $adminMenu;
        $this->entityManager = $entityManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Imports members of Destiny Dance from CSV file to DataBase')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Se incearca sincronizarea bazei de date cu fiserul CSV');

        /**
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }*/

        $this->adminMenu->clearMembersFromDb($this->entityManager);
        $this->adminMenu->importMembersFromCSV($this->entityManager);

        $io->success('Baza de date este sincronizata cu fisierul CSV');

        return 0;
    }
}
