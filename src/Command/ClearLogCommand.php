<?php

namespace App\Command;

use App\Entity\AdminLog;
use App\Repository\AdminLogRepository;
use App\Service\EventService;
use Doctrine\ORM\EntityManagerInterface;
use FontLib\Table\Type\loca;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClearLogCommand extends Command
{
    protected static $defaultName = 'app:log';

    private $eventService;

    private $entityManager;

    /**
     * ClearLogCommand constructor.
     * @param $eventService EventService
     * @param $entityManager EntityManagerInterface
     */
    public function __construct(EventService $eventService, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->eventService = $eventService;
        $this->entityManager = $entityManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Manages admin log entries')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('clear', null, InputOption::VALUE_NONE, 'Clear log')
            ->addOption('list', 'l', InputOption::VALUE_NONE, 'Lists log')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        //$arg1 = $input->getArgument('arg1');

        /**if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }*/

        if ($input->getOption('clear')) {
            $io->title('Clearing Log');
            $this->eventService->clearAdminLog();
            $io->success('Admin Log Cleared');
        }

        if ($input->getOption('list')){
            $io->title('Listing Admin Log Entries');
            self::listItems($input,$output,$this->eventService);
        }

        //$io->success('Admin Log Cleared');


        return 0;
    }

    private static function listItems(InputInterface $input, OutputInterface $output, EventService $eventService)
    {
        $io = new SymfonyStyle($input,$output);
        /**
         * @var $logs AdminLog[]
         */
        $logs = $eventService->getLogs();
        foreach ($logs as $adminLog)
        {
            $io->text([$adminLog->getDate()->format('d-m-Y'),$adminLog->getName(),$adminLog->getAction(),'']);
        }
    }
}
