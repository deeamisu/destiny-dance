<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/11/2020
 * Time: 12:07 AM
 */

namespace App\Service;


use App\Entity\Member;
use App\Entity\PhotoCategory;
use App\Repository\PhotoCategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Finder\Finder;
use League\Csv\Reader;

class Admin
{
    private $passwordEncoder;

    private $parameterBag;

    private $entityManager;

    /**
     * Admin constructor.
     * @param $passwordEncoder
     * @param $parameterBag
     * @param $entityManager
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ParameterBagInterface $parameterBag, EntityManagerInterface $entityManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $admin \App\Entity\Admin
     */
    public function encodePassword($admin)
    {
        $password=$admin->getPassword();
        return $this->passwordEncoder->encodePassword($admin,$password);
    }

    public function getAllImageFiles()
    {
        $dirName = $this->parameterBag->get('kernel.project_dir').'/public/poze';
        $finder = new Finder();
        $finder->files()->in($dirName);
        $finder->sortByName();
        return $finder;
    }

    public function importMembersFromCSV(EntityManagerInterface $entityManager)
    {
        $filepath = $this->parameterBag->get('kernel.project_dir').'/public/membri/Tabel Frds.csv';

        //load the CSV document from a file path
        $csv = Reader::createFromPath($filepath, 'r');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        foreach ($records as $record)
        {
            $member = new Member();
            $member->setName($record['name']);
            $member->setSurname($record['surname']);
            $member->setMIN($record['MIN']);
            $entityManager->persist($member);
        }
        $entityManager->flush();
    }

    public function clearMembersFromDb(EntityManagerInterface $entityManager)
    {
        $members = $this->entityManager->getRepository(Member::class)->findAll();
        foreach ($members as $member)
        {
            $entityManager->remove($member);
            $entityManager->flush();
        }
    }

    public function getGaleryPhotosByCategory()
    {
        $galery = [];
        $photoCategories = $this->entityManager->getRepository(PhotoCategory::class)->findAll();
        foreach ($photoCategories as $photoCategory)
        {
            $galery[$photoCategory->getName()] = $photoCategory->getDancePhotos();
        }
        $galery['noCategory'] = PhotoCategoryRepository::noCategory($this->entityManager);
        return $galery;
    }


}

