<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/24/2020
 * Time: 10:02 PM
 */

namespace App\Service;


use App\Entity\AdminLog;
use App\Event\AdminLogEvent;
use App\EventSubscriber\AdminLogSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class EventService
{
    private $adminLogService;

    private $entityManager;

    /**
     * EventService constructor.
     * @param $adminLogService AdminLogSubscriberService
     * @param $entityManager EntityManagerInterface
     */
    public function __construct(AdminLogSubscriberService $adminLogService, EntityManagerInterface $entityManager)
    {
        $this->adminLogService = $adminLogService;
        $this->entityManager = $entityManager;
    }

    public function dispatchEvent($message)
    {
        $event = new AdminLogEvent($message);
        $subscriber = new AdminLogSubscriber($this->adminLogService);
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber($subscriber);
        $dispatcher->dispatch(AdminLogEvent::NAME,$event);
    }

    public function clearAdminLog()
    {
        $logs = $this->entityManager->getRepository(AdminLog::class)->findAll();
        foreach ($logs as $adminLog)
        {
            $this->entityManager->remove($adminLog);
            $this->entityManager->flush();
        }
    }

    public function getLogs()
    {
        return $this->entityManager->getRepository(AdminLog::class)->findAll();
    }
}