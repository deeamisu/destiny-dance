<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 8/21/2020
 * Time: 2:47 AM
 */

namespace App\Service;

use App\Entity\AdminLog;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AdminLogSubscriberService
{
    private $entityManager;

    private $security;

    private $session;

    /**
     * AdminLogSubscriberService constructor.
     * @param $entityManager EntityManagerInterface
     * @param $security Security
     * @param $session SessionInterface
     */
    public function __construct(EntityManagerInterface $entityManager, Security $security, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->session = $session;
    }

    public function setRecordLogin()
    {
        /**
         * @var $admin \App\Entity\Admin
         */
        $admin = $this->security->getUser();
        $date = new \DateTime('now');

        $adminLog = new AdminLog();
        $adminLog->setName($admin->getUsername())
            ->setDate($date)
            ->setAction('logged in');

        $this->entityManager->persist($adminLog);
        $this->entityManager->flush();

        $this->session->set('adminName',$admin->getUsername());
    }

    public function adminAction($message)
    {
        $adminName = $this->session->get('adminName');
        $date = new \DateTime('now');
        $adminLog = new AdminLog();
        $adminLog->setName($adminName)
            ->setDate($date)
            ->setAction($message);
        $this->entityManager->persist($adminLog);
        $this->entityManager->flush();
    }
}