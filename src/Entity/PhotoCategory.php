<?php

namespace App\Entity;

use App\Repository\PhotoCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotoCategoryRepository::class)
 */
class PhotoCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=DancePhotos::class, mappedBy="category")
     */
    private $dancePhotos;

    public function __construct()
    {
        $this->dancePhotos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|DancePhotos[]
     */
    public function getDancePhotos(): Collection
    {
        return $this->dancePhotos;
    }

    public function addDancePhoto(DancePhotos $dancePhoto): self
    {
        if (!$this->dancePhotos->contains($dancePhoto)) {
            $this->dancePhotos[] = $dancePhoto;
            $dancePhoto->setCategory($this);
        }

        return $this;
    }

    public function removeDancePhoto(DancePhotos $dancePhoto): self
    {
        if ($this->dancePhotos->contains($dancePhoto)) {
            $this->dancePhotos->removeElement($dancePhoto);
            // set the owning side to null (unless already changed)
            if ($dancePhoto->getCategory() === $this) {
                $dancePhoto->setCategory(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }


}
